
import React, {Fragment} from "react";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from "react-navigation-stack";
import { Splash, Login, Home, OrderDetail1, OrderDetail4, LoginSignUpPrompt } from './src/Screens';

const AppNavigator = createStackNavigator({

  SplashScreen: Splash,
  LoginScreen: Login,
  HomeScreen: Home,
  Order1: OrderDetail1,
  Order4: OrderDetail4,
  LoginPrompt: LoginSignUpPrompt 

},
{
  initialRouteName: "LoginScreen",
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#FECE00',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
}

);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };


render() {
  return (

    <AppContainer />

  )
 }
}
