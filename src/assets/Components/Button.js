import React from 'React';
import {View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
        return (
            <TouchableOpacity onPress = {onPress} style = {styles.myButton}>
                <Text style = {styles.TextStyle}>
                    {children}
                </Text>
            </TouchableOpacity>


)
    }

const styles = StyleSheet.create({

      myButton:{
         height: 47,
         width: 297,  //The Width must be the same as the height
        borderRadius:100, //Then Make the Border Radius twice the size of width or Height   
        backgroundColor:'#FECE00',
      },
      TextStyle: {
          
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10,
        color: '#FCFDFF'
      }
})

export { Button }