import React from 'React';
// import {View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import {Modal, TouchableHighlight, Alert,View, StyleSheet, Text} from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
// import Modal from "react-native-modal";

class LoginSignUpPrompt extends React.Component {

  state = {
    modalVisible: false,
 }
 toggleModal(visible) {
    this.setState({ modalVisible: visible });
 }
    // constructor(props) {
    //     super(props)
    //     this.state = {
    //       modalVisible: false,
    //       // visible:false
    //     }
    //   }

    //   setModalVisible(visible) {
    //     this.setState({modalVisible: visible});
    //   }

    render() {

        return (

        <View>
         {/* <View style = {styles.container}> */}
          <Modal animationType = {"slide"} transparent = {false}

             visible = {this.state.modalVisible}
             onRequestClose = {() => { console.log("Modal has been closed.") } }>
             
             <View style = {styles.modal}>
                <Text style = {styles.text}>Modal is open!</Text>
                
                <TouchableHighlight onPress = {() => {
                   this.toggleModal(!this.state.modalVisible)}}>
                   
                   <Text style = {styles.text}>Close Modal</Text>
                </TouchableHighlight>
             </View>
          </Modal>
          
          {/* <TouchableHighlight onPress = {() => {this.toggleModal(true)}}>
             <Text style = {styles.text}>Open Modal<s/Text>
          </TouchableHighlight> */}
       </View>
    )

        //   <View style={{marginTop: 22}}>
        //   <Modal
        //     animationType="slide"
        //     transparent={false}
        //     visible={this.state.modalVisible}
        //     onRequestClose={() => {
        //       Alert.alert('Modal has been closed.');
        //     }}>
        //     <View style={{marginTop: 22}}>
        //       <View>
        //         <Text>Hello World!</Text>
  
        //         <TouchableHighlight
        //           onPress={() => {
        //             this.setModalVisible(!this.state.modalVisible);
        //           }}>
        //           <Text>Hide Modal</Text>
        //         </TouchableHighlight>
        //       </View>
        //     </View>
        //   </Modal>
  
        //   <TouchableHighlight
        //     onPress={() => {
        //       this.setModalVisible(true);
        //     }}>
        //     <Text>Show Modal</Text>
        //   </TouchableHighlight>
        // </View>
        // )
        // // return (

        // //     <Modal onBackdropPress={this.hide} isVisible={this.props.visible} style={styles.container}>
        // //     </Modal>
        // //     );
        }
    }
    const styles = StyleSheet.create ({
      container: {
         alignItems: 'center',
         backgroundColor: '#ede3f2',
         padding: 100
      },
      modal: {
         flex: 1,
         alignItems: 'center',
         backgroundColor: '#f7021a',
         padding: 100
      },
      text: {
         color: '#3f2949',
         marginTop: 10
      }
   })
export { LoginSignUpPrompt }    