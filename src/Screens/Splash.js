import React from 'React';
import {View, StyleSheet, Image} from 'react-native';

class Splash extends React.Component {
    
     static navigationOptions = {
          header: null,
        }
      
    render() {
        return (
            <View>

                <View style = {styles.container1}>
                    <Image source = {require('../assets/images/sideDesign.png')} />
                </View>

                <View style = {styles.container2} >
                <Image source = {require('../assets/images/mainLogo.png')} />
                </View>

                <View style = {styles.container3}>
                    <Image source = {require('../assets/images/DownImage.png')}/>
                </View>

            </View>    
        );
    }
}

const styles = StyleSheet.create({

    container1: {
        width: 10,
        height: 10
      },

    container2: {
        marginTop: 120,
        height: 200, width: 230,
        padding: 80
      },
      
    container3: {
        marginTop: 100,
        width: 10
    }  
})

export { Splash }