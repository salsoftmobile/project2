import React from 'React';
import {View, StyleSheet, Image, Text, TouchableOpacity, TextInput, CheckBox } from 'react-native';
// import CheckBox from 'react-native-check-box';
// import RoundCheckbox from 'react-native-round-checkbox';
//import { CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

class OrderDetail1 extends React.Component {

    NextClick = () => {

        this.props.navigation.navigate('Order4')
    }

    render() {
        return (

            <View>
                <ScrollView>
                <View style ={{alignItems: "center", justifyContent: "center", paddingRight: 80, width: "120%", height: 180, marginTop: -40 }}>
                    <View>
                        <Image 
                        style ={{}}
                        source = {require('../assets/images/OrderDetail-1/Icons.png')}/>
                    </View>
                </View>

                    <View style ={{ marginTop: -18, paddingLeft: 20, flexDirection: "row" }}>
                        <Text style= {{ fontWeight: "bold", fontSize: 18}}>
                            YOUR NEW
                        </Text>

                        <Text style ={{ fontWeight: "bold", color: '#FECE00', fontSize: 18}}>
                            ADDRESS
                        </Text>
                    </View>

                    <View style ={{ marginTop: 25, paddingLeft: 38}}>
                        <Text style= {{ color: '#666666'}}>
                            Enter new zipcode
                        </Text>
                    </View>

                    <View style={{ paddingRight: 59, marginTop: 15, marginLeft: 29, marginRight: 13}}>
                        <View style ={ styles.InputContainer }>
                            <Image
                            style = {{marginLeft: 15, paddingLeft: 5, heigth: 10 }}
                            source = {require('../assets/images/OrderDetail-1/zipCodeIcon.png')}>
                            </Image>

                            <TextInput
                            style = {{ paddingLeft: 12}} 
                            placeholder = "123*"/>
                        </View>    
                    </View>

                    <View style ={{ marginTop: 23, paddingLeft: 38}}>
                        <Text style= {{ color: '#666666'}}>
                            Enter new address
                        </Text>
                    </View>

                    <View style={{ paddingRight: 59, marginTop: 15,  marginLeft: 29, marginRight: 13}}>
                        <View style ={ styles.InputContainer }>
                            <Image
                            style = {{ marginLeft: 15, padding: 5, heigth: 10 }}
                            source = {require('../assets/images/OrderDetail-1/LocationIcon.png')}>
                            </Image>

                            <TextInput
                            style = {{ paddingLeft: 12}} 
                            placeholder = "Enter Address"/>
                        </View>    
                    </View>

                    <View>
                        <Image
                        style = {{ paddingTop: 1, height: 2, marginTop: 40, width: 370, marginLeft: 20}} 
                        source ={require('../assets/images/OrderDetail-4/divider.png')}>
                        </Image>
                    </View>

                    <View style ={{ marginTop: 30, paddingLeft: 20 }}>
                    <Image
                        style ={{ height: 20, width: "70%"}}
                        source = {require('../assets/images/OrderDetail-1/text4.png')}/>
                    </View>

                    <View style = {{ marginTop: 22, flexDirection: "row", paddingLeft: 43}}>
                        <View style = {{width: "27%", height: 54, flexDirection: "row"  }}>

                         
                        <CheckBox
                          checkedIcon='dot-circle-o'
                          uncheckedIcon='circle-o'
                        //checked={this.state.checked}
                        />
    
                        <Text style ={{ color: '#666666', paddingTop: 6}}>
                            House
                        </Text>
                        </View>

                        <View style = {{width: "27%", height: 54, flexDirection: "row"}}> 

                        <CheckBox />
                        <Text style ={{ color: '#666666', paddingTop: 6}}>
                            Cottage
                        </Text>
                        </View>

                        <View style = {{width: "27%", height: 54, flexDirection: "row" }}>
                            <CheckBox/>
                        <Text style ={{ color: '#666666', paddingTop: 6}}>
                            Bungalow
                        </Text>
                        </View>
                    </View>

                    <View style = {{ marginTop: -3, alignItems: "center", flexDirection: "row", paddingLeft: 43}}>
                        <View style = {{width: "30%", height: 54, flexDirection: "row"  }}>
                            <CheckBox />
                        <Text style ={{ color: '#666666', paddingTop: 6}}>
                            Mansion
                        </Text>
                        </View>

                        <View style = {{width: "30%", height: 54, flexDirection: "row"  }}> 
                        <CheckBox/>
                        <Text style ={{ color: '#666666', paddingTop: 6}}>
                            Apartment
                        </Text>
                        </View>

                    </View>

                    <View style ={{ marginTop: 15, paddingLeft: 34 }}>
                        <Text
                        style ={{ height: 20, width: "50%", color: '#666666'}}>
                            Floor Levels
                        </Text>
                    </View>

                    <View>
                        <Image
                        style = {{ paddingTop: 1, height: 2, marginTop: 33, width: 370, marginLeft: 20}} 
                        source ={require('../assets/images/OrderDetail-4/divider.png')}>
                        </Image>
                    </View>

                    <View style ={{ marginTop: 40, alignItems: "center", flexDirection: "row", justifyContent: "center", paddingLeft: 46 }}>
                    <View style ={{ width: "47%", height: 54  }}>
                       <TouchableOpacity
                       style ={{ }}>
                           <Image 
                           source = {require('../assets/images/OrderDetail-1/BackButton.png')}/>
                       </TouchableOpacity>
                    </View>

                    <View style ={{ width: "50%", height: 54 }}>
                       <TouchableOpacity
                       onPress = {() => this.NextClick()}>
                          
                      <Image source = {require('../assets/images/OrderDetail-1/NextButton.png')}/>
                       </TouchableOpacity>
                    </View>
                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    InputContainer: { 
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        borderRadius:100,
        flexDirection: 'row',
        height: 45, 
        alignItems:"center",
        width: "115%", 
        borderColor: "gray", 
        elevation: 3
      },
})

export { OrderDetail1 }