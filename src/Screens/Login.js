import React from 'React';
import { View, StyleSheet, Image, Text, TextInput,TouchableOpacity} from 'react-native';
import {Button} from '../assets/Components/Button';
import * as Font from 'expo-font';
//import { Icon } from 'react-native-elements';

class Login extends React.Component {

    // state = {
    //     fontLoaded: false,
    //   };

    static navigationOptions = {
        header: null,
      }

LoginButtonClick = () =>{
 
    this.props.navigation.navigate('HomeScreen')
 
  }

//   async componentDidMount() {
//     await Font.loadAsync({
//       'Font Awesome 5 Free-Regular-400' : require('./src/assets/fonts/fontawesome-free-5.10.2-desktop/otfs/Font Awesome 5 Free-Regular-400')
//     });

//     this.setState({ fontLoaded: true });
//   }

    render() {
        return (
            <View>

                {/* <View style = {{ paddingRight: 0, marginTop: -20, paddingLeft: 10, width: 100}}>
                    <Image source ={require('../assets/images/login1.png')}>

                    </Image>
                </View> */}

                <View style = {styles.container2}>
                    <Image source = {require('../assets/images/mainLogo.png')}/>
                </View>

                {/* <View style = {styles.LoginText}>
                 {
                  this.state.fontLoaded ? ( 
                  <Text style ={{  fontFamily: "fontawesome-free-5.10.2-desktop", fontSize : 20, fontWeight: "bold"}}>
                    Login
                </Text>
                  ) : null
                 }
                </View> */}

               <View style = {styles.LoginText}>

                  <Text style ={{  fontSize : 20, fontWeight: "bold"}}>
                    Login
                  </Text>

                </View>


                <View style={{alignItems:"center", justifyContent:"center"}}>
                <View style = {styles.InputContainer}>
    
                <Image style = {{ marginLeft: 12, padding: 5, heigth: 10}}
                    source = {require('../assets/images/UserIcon.png')}>
                </Image> 
                

                <TextInput
                style = {{ marginLeft: 5}} 
                placeholder = "Email Address"/>
                </View>

                <View style = {styles.InputContainer2}>

                <Image style = {{ marginLeft: 12, padding: 5, heigth: 10}}
                    source = {require('../assets/images/passwordLock.png')}>
                </Image> 

                <TextInput
                style = {{ paddingLeft: 10}} 
                placeholder = "Password"/>
                </View>
                </View>


                <View style ={styles.TextAlignRight}>
                    <Text style = {{ color: '#999999', fontSize : 13}}>
                        Forgot Password?
                    </Text>
                </View>


                <View style = {{ padding: 12, paddingLeft: 60, flexDirection: 'row'}}>
                <Button
                onPress = {() =>  this.LoginButtonClick()} >
                    Login
            
                </Button>
                </View>


                <View style = {{ paddingLeft: 120, flexDirection: "row" }}>
                    <Text style = {{ color : '#999999'}}>
                        Don't have an account?
                    </Text>

                    <Text style = {styles.RegisterStyle}>
                        Register
                    </Text>
                </View>
                
                <View style = {{ height: 10, paddingLeft: 40, width: "30%", flex: 1}}>
                    <Image source = {require('../assets/images/DownImage.png')}/>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({

    container2: {
        flex: 1,
        marginTop: 20,
        padding: 80
    },
    LoginText: {
        padding: 65,
        justifyContent: "center",
        alignItems: "center",
    },
    InputContainer: { 
        borderRadius:100,
        flexDirection: 'row',
        height: 45, 
        alignItems:"center",
        width: "75%", 
        borderColor: "gray", 
        borderWidth: 0.5
      },
      InputContainer2: {
        margin: 15,
        borderRadius:100,  
        flexDirection: 'row',
        height: 45, 
        alignItems:"center",
        width: "75%", 
        borderColor: "gray", 
        borderWidth: 0.5
      },
    InputStyle: 
        { height: 40, width: "75%", borderColor: "gray", borderWidth: 1},
    TextAlignRight : {
        marginTop: -14,
        padding: 7,
        flexDirection: 'row',
        paddingLeft: 280,
    },
    
    RegisterStyle : {
        color: "#FECE00",
        textDecorationLine: 'underline'
    },
    
  SubmitButtonStyle: {
 
    marginTop:10,
    paddingTop:15,
    paddingBottom:15,
    marginLeft:30,
    marginRight:30,
    backgroundColor:'#00BCD4',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
})

export { Login };