import React from "react";
import { View, Image, TouchableOpacity } from 'react-native';

class DrawerImage extends React.Component {          

    
toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };
  
    render() {
        return (
          <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
            <Image
              source={require('../assets/images/drawer.png')}
              style={{ width: 25, height: 40, marginLeft: 7 }}
            />
          </TouchableOpacity>
        </View>

        );
    }
}        

export { DrawerImage};