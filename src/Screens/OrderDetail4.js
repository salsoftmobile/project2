import React from 'React';
import {View, StyleSheet, Image, Text, TouchableOpacity, TouchableHighlight, Modal } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { LoginSignUpPrompt } from './LoginSignUpPrompt';
import {Button} from '../assets/Components/Button';
// import Modal from "react-native-modal";

class OrderDetail4 extends React.Component {
    clicka = () => {
        console.log('this.second' ,this.second);
        this.second.show();
        this.toggleModal(true)
        
    }

    state = {
        modalVisible: false,
     }
     toggleModal(visible) {
        this.setState({ modalVisible: visible });
     }

     show = () => {

        <LoginSignUpPrompt />
     }

    render() {
        
        return (

            <View>
              <LoginSignUpPrompt ref={(r) => this.second = r} />
                <ScrollView>
                <View style ={{alignItems: "center", justifyContent: "center", paddingRight: 80, width: "120%", height: 180, marginTop: -40 }}>
                    <View>
                        <Image 
                        style ={{}}
                        source = {require('../assets/images/OrderDetail-1/Icons.png')}/>
                    </View>
                </View>
                
                    <View style ={{ marginTop: -18, paddingLeft: 30 }}>
                        <Image
                        style ={{ height: 15, width: "30%"}}
                        source = {require('../assets/images/OrderDetail-4/text1.png')}/>
                    </View>

     <View style = {{ flexDirection: "row"}}>    
                <View style={styles.container}>
                    <View style={styles.myButton}>
                    <Text 
                    style = {{ color: '#FFFFFF'}}>03</Text>
                    </View>

                    <Text
                    style = {{color: '#666666', fontSize: 12, marginTop: 5}}>
                        Number 
                    </Text>

                    <Text
                    style = {{color: '#666666', fontSize: 12}}>
                        of Bedrooms
                    </Text>
                </View>

                <View style={styles.container}>
                    <View style={styles.myButton}>
                    <Text 
                    style = {{ color: '#FFFFFF'}}>03</Text>
                    </View>

                    <Text
                    style = {{color: '#666666', fontSize: 12, marginTop: 5}}>
                        Packaging
                    </Text>

                    <Text
                    style = {{color: '#666666', fontSize: 12}}>
                        Items
                    </Text>
                </View>

            <View style={styles.container}>
                    <View style={styles.myButton}>
                    <Text 
                    style = {{ color: '#FFFFFF'}}>03</Text>
                    </View>

                    <Text
                    style = {{color: '#666666', fontSize: 12, marginTop: 5}}>
                        Fragile
                    </Text>

                    <Text
                    style = {{color: '#666666', fontSize: 12}}>
                        Items
                    </Text>
                </View>
           </View> 


         <View style = {styles.container2}>
                    <View styles = {{ width: "40%"}}>
                         <Image 
                          styles = {{ width: "40%" }}
                          source = {require('../assets/images/OrderDetail-4/circle1.png')}/>
                    </View>

                    <View styles = {{ width: "20%"}}>
                         <Image 
                          source = {require('../assets/images/OrderDetail-4/circle2.png')}/>
                    </View>

                    <View styles = {{ width: "20%"}}>
                         <Image 
                          source = {require('../assets/images/OrderDetail-4/circle3.png')}/>
                    </View>
         </View>

         <View style ={{ marginTop: 60, paddingLeft: 30 }}>
                        <Image
                        style ={{ height: 18, width: "34%"}}
                        source = {require('../assets/images/OrderDetail-4/text2.png')}/>
         </View>


                    <View style = {{ marginTop: 19, alignItems: "center", justifyContent: "center"}}>
                            <Image 
                            style = {{width: "84%", height: 60 }}
                            source = {require('../assets/images/OrderDetail-4/text3.png')}/>
                    </View>

                    <View>
                        <Image
                        style = {{ paddingTop: 2, height: 2, marginTop: 35, width: 370, marginLeft: 20}} 
                        source ={require('../assets/images/OrderDetail-4/divider.png')}>
                        </Image>
                    </View>

                    <View style ={{ marginTop: 35, alignItems: "center", flexDirection: "row", justifyContent: "center", paddingRight: 79 }}>
                    <View style ={{ width: "36%", height: 54  }}>
                       <TouchableOpacity
                       style ={{ }}>
                           <Image 
                           source = {require('../assets/images/OrderDetail-4/Button1.png')}/>
                       </TouchableOpacity>
                    </View>

                    <View style ={{ width: "34%", height: 54 }}>
                       <TouchableOpacity
                        onPress={ () => {this.toggleModal(true)}}
                        // onPress={ () => {this.clicka()}}
                       >
                           <Image 
                           source = {require('../assets/images/OrderDetail-4/Button2.png')}/>
                       </TouchableOpacity>
                    </View>
                    </View>                 



           <View style = {{ height: 50, width: 50 }}>
                    <Modal animationType = {"slide"} transparent = {false}

                   visible = {this.state.modalVisible}
                   onRequestClose = {() => { console.log("Modal has been closed.") } }>
 
          <View style={{
                  backgroundColor: 'rgba(100,100,100, 0.5)',
            //    backgroundColor: 'rgba(52, 52, 52, 0.8)',
            //    backgroundColor: '#00000080',
            //    opacity: '50%',
                      flex: 1,
                      flexDirection: 'column',
                      justifyContent: 'center',
                       alignItems: 'center'}}> 
               <View style={{
                   backgroundColor: '#fff',
                   borderRadius:20,
                   width: 320,
                   height: 400}}>

                   {/* <View style = {styles.modal}>
                   <Text style = {styles.text}>Modal is open!</Text> */}
   
                    <TouchableHighlight 
                    style= {{ paddingLeft: 292, paddingTop: 12}} 
                    onPress = {() => {
                     this.toggleModal(!this.state.modalVisible)}}>

                         <Image source = {require('../assets/images/OrderDetail-4/closeButton.png')}>
                         </Image>
                   </TouchableHighlight>

                   <View style = {styles.LoginText}>

                   <Text style ={{  fontSize : 20, fontWeight: "bold"}}>
                    Login
                   </Text>
                   </View>

           <View style={{alignItems:"center", justifyContent:"center", marginTop: -39}}>
                <View style = {styles.InputContainer}>
    
                <Image style = {{ marginLeft: 12, padding: 5, heigth: 10}}
                    source = {require('../assets/images/UserIcon.png')}>
                </Image> 
                

                <TextInput
                style = {{ marginLeft: 5}} 
                placeholder = "Email Address"/>
                </View>

                <View style = {styles.InputContainer2}>

                <Image style = {{ marginLeft: 12, padding: 5, heigth: 10}}
                    source = {require('../assets/images/passwordLock.png')}>
                </Image> 

                <TextInput
                style = {{ paddingLeft: 10}} 
                placeholder = "Password"/>
                </View>
          </View>

          
                <View style = {{ padding: 12, flexDirection: 'row', alignItems: "center", marginLeft: 14}}>
                  <TouchableOpacity 
                    style = {{ height: 42,
                        width: 272,  
                       borderRadius:100,  
                       backgroundColor:'#FECE00',}}
                    onPress = {() =>  this.LoginButtonClick()} >
                  <Text
                  style ={{ alignSelf: 'center',
                  fontSize: 16,
                  fontWeight: '600',
                  paddingTop: 8,
                  paddingBottom: 10,
                  color: '#FCFDFF'}}>    
                    Login
                   </Text>   
                  </TouchableOpacity>
               </View>

               
               <View style = {{ paddingLeft: 70, flexDirection: "row", alignItems: "center" }}>
                    <Text style = {{ color : '#999999'}}>
                        Don't have an account?
                    </Text>

                    <Text style = {styles.RegisterStyle}>
                        Register
                    </Text>
                </View>

      </View>
                   </View>
                   </Modal>
               </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    LoginText: {
        padding: 60,
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        flex: 1,
        width: "10%",
        marginTop: 50,
        paddingLeft: 47,
      },
      container2: {
        flex: 1,
        flexDirection: "row",
        width: "40%",
        marginTop: 50,
        paddingLeft: 32,
      },

    RegisterStyle : {
        color: "#FECE00",
        textDecorationLine: 'underline'
    },
      myButton:{
        alignItems: "center",
        padding: 10,
        height: 40,
        width: 40,  //The Width must be the same as the height
        borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
        backgroundColor:'#000000',
    
      },
     modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#f7021a',
        padding: 100
     },
     text: {
        color: '#3f2949',
        marginTop: 10
     },
     InputContainer: { 
        borderRadius:100,
        flexDirection: 'row',
        height: 40, 
        alignItems:"center",
        width: "85%", 
        borderColor: "gray", 
        borderWidth: 0.15
      },
      InputContainer2: {
        margin: 17,
        borderRadius:100,  
        flexDirection: 'row',
        height: 40, 
        alignItems:"center",
        width: "85%", 
        borderColor: "gray", 
        borderWidth: 0.15
      }
})

export { OrderDetail4 }