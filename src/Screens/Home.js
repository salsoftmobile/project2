import React, {Fragment} from 'React';
import { View, StyleSheet, Image, Text, ImageBackground, TextInput, TouchableOpacity } from 'react-native';
import { ScrollView} from 'react-native-gesture-handler';
import { DrawerImage } from './DrawerImage';
import { HeaderTitle } from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation';

const DrawerNavigatorExample = createDrawerNavigator({
  //Drawer Optons and indexing
  Screen1: { //Title
    //screen: FirstActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: "Demo Screen 1"
    }
  },
  Screen2: {//Title
    //screen: Screen2_StackNavigator,
    navigationOptions: {
      drawerLabel: "Demo Screen 2"
    }
  },
  Screen3: {//Title
    //screen: Screen3_StackNavigator,
    navigationOptions: {
      drawerLabel: "Demo Screen 3"
    }
  },
});
export default createAppContainer(DrawerNavigatorExample);

class Home extends React.Component {

  static navigationOptions = {
  
    headerLeft:
      <DrawerImage navigationProps={navigation} />

  }

  HomeButtonClick = () =>{
 
  this.props.navigation.navigate('Order1')

}

    render() {
        return (

          <View> 
            <ScrollView>
                <View style ={{alignItems: "center", justifyContent: "center", paddingRight: 80, width: "120%", height: 180, marginTop: -40 }}>

                  <Image 
                  style={{flexDirection: "row", width: "100%", height: 180}}
                  source={require('../assets/images/HomeGrp2.png')} >

                  </Image>
                {/* <ImageBackground style={{flexDirection: "row", width: "90%", height: 90}}
                 source={require('../assets/images/HomeImage.png')} >

                    
                <View style= {{ width: "50%"}}>
                <Image 
                 source={require('../assets/images/BlurImage.png')}> 
                </Image>
                </View>

                <View >
                <Image 
                 style = {{paddingRight: 100, width: "30%", heigth: 10}}
                 source={require('../assets/images/HomeImg2.png')}> 
                </Image>
                </View>

                </ImageBackground> */}
                
                </View>


                <View style = {{padding: 28, width: "10%", height: 5 }}>
                  <Image
                  style ={{  marginTop: -20, paddingLeft: 60 }} 
                  source = {require('../assets/images/HomeText1.png')}/>
                </View>
                
                <View style = {{ padding: 60, alignItems: "center"}}>
                  <Image
                  style = {{ marginTop: -79, paddingRight: 20}}
                  source = {require('../assets/images/textInputImage.png')}/>
                </View>

                <View style = {{ marginTop: -54, flexDirection: "row", justifyContent: "center", paddingLeft: 90  }}>
                <View style = {{ width: "45%"}}>
                  <Image 
                  style = {{ width: "37%", height: 52 }}
                  source = {require('../assets/images/HouseImg.png')}/>

                  <Text
                  style ={{fontSize: 12, fontWeight: "bold"}}> 
                  HOUSE 
                  </Text>
                </View>

                <View style = {{ width: "45%" }}>
                  <Image 
                  style = {{width: "37%", height: 52  }}
                  source = {require('../assets/images/building.png')}/>

                  <Text
                  style ={{fontSize: 12, fontWeight: "bold"}}>
                  APARTMENT 
                  </Text>
                </View>
                </View>

                <View style = {{ marginTop: 57, flexDirection: "row", justifyContent: "center", paddingLeft: 90  }}>
                <View style = {{ width: "45%"}}>
                  <Image 
                  style = {{ width: "37%", height: 52 }}
                  source = {require('../assets/images/CottageImg.png')}/>

                  <Text
                  style ={{fontSize: 12, fontWeight: "bold"}}> 
                   COTTAGE 
                  </Text>
                </View>

                <View style = {{ width: "45%" }}>
                  <Image 
                  style = {{width: "37%", height: 52  }}
                  source = {require('../assets/images/MansionImg.png')}/>

                  <Text
                  style ={{fontSize: 12, fontWeight: "bold"}}>
                  MANSION
                  </Text>
                </View>
                </View>

                <View style = {{padding: 32, marginTop: 30}}>
                  <Image
                  style = {{ height: 19, width: "30%"}} 
                  source = {require('../assets/images/PROMOTIONS.png')}/>
                </View>

                <View style = {{padding: 10, marginTop: -23, paddingRight: 33}}>
                  <Image
                  style = {{ height: 130, width: "110%"}} 
                  source = {require('../assets/images/PromoImg.png')}/>
                </View>

                <View style = {{padding: 10, marginTop: 10, paddingRight: -13,  alignItems: "center"}}>
                  <Image
                  style = {{ height: 40, width: "74%"}} 
                  source = {require('../assets/images/HomeText2.png')}/>
                </View>

                <View style = {{padding: 5, marginTop: 14,  alignItems: "center", justifyContent: "center"}}>
                  <TouchableOpacity
                  style = {{ height: 45, width: "60%"}} 
                    onPress = {()=> this.HomeButtonClick()}>
                  <Image
                  source = {require('../assets/images/Req2.png')}/>
                  </TouchableOpacity>
                </View>

                <View style = {{ alignItems: "center", justifyContent: "center"}}>
                <View style = {{padding: 5, marginTop: -3, flexDirection: "row", height: 180, width: "110%"}}>
                  <Image
                  style = {{}} 
                  source = {require('../assets/images/HomeLastImg.png')}/>

                  <Image
                  style ={{ paddingRight: 50}}
                    source ={require('../assets/images/BlurHomeImg2.png')}>
                  </Image>
                </View>

                <View style = {{ padding: 30}}>
                <Image
                   source ={require('../assets/images/homelst.png')}>
                  </Image>
                </View>
                </View>

                </ScrollView>
          </View>    

);
    }
}

const styles = StyleSheet.create({

  container2: {
   
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

  },
  
  SubmitButtonStyle: {
 
    marginTop:10,
    paddingTop:15,
    paddingBottom:15,
    marginLeft:30,
    marginRight:30,
    backgroundColor:'#00BCD4',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
}
)
export { Home };    